FROM mysql:5.7

COPY ./dump/database.sql /tmp/
COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

# Allow empty root password
ENV MYSQL_ALLOW_EMPTY_PASSWORD 1

# Ensure creation of database for the project + configure
# user with the right password
ENV MYSQL_USER test0
ENV MYSQL_PASSWORD test0_pw
ENV MYSQL_DATABASE test0

# Ensure import of the database file
ENV MYSQL_IMPORT_DATABASE /tmp/database.sql