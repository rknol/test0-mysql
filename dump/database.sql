USE test0;

CREATE TABLE customers (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) UNIQUE NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    gender VARCHAR(1) NOT NULL,
    country VARCHAR(2) NOT NULL,
    balance INT,
    balance_bonus INT,
    bonus_pct INT
) ENGINE=InnoDB;


CREATE TABLE transactions (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL,
    event_date DATETIME,
    country VARCHAR(2),
    amount INT,
    bonus INT,
    FOREIGN KEY (user_id) REFERENCES customers(id) ON DELETE CASCADE
) ENGINE=InnoDB;